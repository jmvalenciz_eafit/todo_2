<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller{
    /**
     * Returns a list of Tasks
     *
     * @return Response
     */
    public function index(){
        $data = Task::select('id', 'title', 'content')
              ->where("user_id", Auth::id())
              ->get();
        return Inertia::render('Dashboard',['data'=>$data]);
    }

    /**
     * Creates a new Task given a title and a content
     *
     * @return Response
     */
    public function store(Request $request){
        $data = $request->all();
        Validator::make($data, [
            'title' => ['required'],
            'content' => ['required']
        ])->validate();
        $data["user_id"] = Auth::id();
        Task::create($data);
        return redirect()->back()
                         ->with('message', 'Task Created Successfully.');
    }

    /**
     * Updates a specific Task that have a given number
     *
     * @return Response
     */
    public function update(Request $request){
        $request->offsetUnset('_method');
        Validator::make($request->all(), [
            'title' => ['required'],
            'content' => ['required']
        ])->validate();
        if ($request->has('id')) {
            Task::find($request->input('id'))->where('id', $request->input('id'))->where("user_id", Auth::id())->update($request->all());
            return redirect()->back()
                             ->with('message', 'Task Updated Successfully.');
        }
    }

    /**
     * Deletes a given Task.
     *
     * @return Response
     */
    public function destroy(Request $request){
        if ($request->has('id')) {
            Task::where("id", $request->input('id'))
                ->where("user_id", Auth::id())
                ->delete();
            return redirect()->back();
        }
    }

}
